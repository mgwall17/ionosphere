import {
  Dashboard as DashboardIcon,
  Help as HelpIcon,
  Lock as LockIcon,
  VpnKey as VpnKeyIcon,
  Cloud as CloudIcon,
  WebAsset as WebAssetIcon,
  Layers as LayersIcon,
  Widgets as WidgetsIcon,
} from "@material-ui/icons";

const menuItems = [
  {
    label: "Dashboard",
    icon: <DashboardIcon fontSize="large" />,
    path: "/dashboard",
    items: [],
    // items: [
    //   { label: "Manage Service Quotas", path: "/services/quotas" },
    //   { label: "Maintenance Calendar", path: "/services/calendar" }
    // ]
  },
  {
    label: "Workspaces",
    icon: <WebAssetIcon fontSize="large" />,
    path: "/workspaces",
    items: [],
  },
  {
    label: "Assets",
    icon: <LayersIcon fontSize="large" />,
    path: "/assets",
    items: [],
  },
  {
    label: "Clouds",
    icon: <CloudIcon fontSize="large" />,
    path: "/clouds",
    items: [],
  },
  {
    label: "Credentials",
    icon: <VpnKeyIcon fontSize="large" />,
    path: "/credentials",
    items: [],
  },
  {
    label: "Templates",
    icon: <WidgetsIcon fontSize="large" />,
    path: "/templates",
    items: [],
  },
  {
    label: "Support",
    icon: <HelpIcon fontSize="large" />,
    path: "/support",
    items: [
      {
        label: "Getting Started",
        path: "https://learning.cyverse.org",
        description:
          "Help on creating account and learning what services are available.",
      },
      {
        label: "Tutorials",
        path: "https://learning.cyverse.org/en/latest/tutorials.html",
        description: "Tutorials covering CyVerse services.",
      },
      {
        label: "Focus Forum Webinars",
        path: "https://learning.cyverse.org/en/latest/webinars.html",
        description: "Upcoming webinars on how to use CyVerse services.",
      },
      {
        label: "Policies",
        path: "https://cyverse.org/policies",
        description: "CyVerse policies that apply to all users.",
      },
      {
        label: "Wiki",
        path: "https://cyverse.atlassian.net/wiki",
        description: "A space for collaboration.",
      },
      {
        label: "FAQ",
        path: "https://learning.cyverse.org/projects/faq",
        description: "Answers to frequenty asked questions about CyVerse",
      },
    ],
  },
  {
    label: "Administrative",
    icon: <LockIcon fontSize="large" />,
    path: "/administrative",
    items: [
      {
        label: "Users",
        icon: <LockIcon fontSize="small" color="primary" />,
        path: "/administrative/users",
        description:
          "Search across all CyVerse users and view details about individual users.",
      },
      {
        label: "Restricted Usernames",
        icon: <LockIcon fontSize="small" color="primary" />,
        path: "/administrative/usernames",
        description: "Show and edit restricted usernames.",
      },
      {
        label: "Access Requests",
        icon: <LockIcon fontSize="small" />,
        path: "/administrative/requests",
        description:
          "Search across all access requests and view/deny/approve individual requests.",
      },
      {
        label: "Workshops",
        icon: <LockIcon fontSize="small" />,
        path: "/administrative/workshops",
        description: "View, create, and modify workshops.",
      },
      {
        label: "Form Submissions",
        icon: <LockIcon fontSize="small" />,
        path: "/administrative/submissions",
        description:
          "Search across all form submissions and view individual submissions.",
      },
      {
        label: "Manage Forms",
        icon: <LockIcon fontSize="small" />,
        path: "/administrative/forms",
        description: "View and edit forms.",
      },
    ],
  },
];

const getMenuItem = (label) => {
  return menuItems.find((item) => item.label === label);
};

export { menuItems, getMenuItem };
