import {
    Fab
  } from "@material-ui/core";
  import AddIcon from '@material-ui/icons/Add'
  import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    fabButton: {
      position: "absolute",
      margin: '0 auto',
      right: '5%',
      top: '10%',
    },
    }));
    
  function AddButton() {
    const classes = useStyles();

    return (
        <Fab color="secondary" aria-label="add" variant="extended" className={classes.fabButton}>
            Actions
        </Fab>
    );
}
  
export default AddButton