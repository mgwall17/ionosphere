import {
    Card,
    CardHeader,
    CardContent,
    CardActions,
    Typography,
    Avatar,
    Button,
  } from "@material-ui/core";
 
import Jdenticon from 'react-jdenticon';
import React from 'react';

  import { makeStyles } from '@material-ui/core/styles'

  const useStyles = makeStyles((theme) => ({
    content: {
      height: '6em',
    },
    title: {
      lineHeight: '1.1',
      fontSize: '1.4em',
    },
    avatar: {
      backgroundColor: '#ffff',
    },
  }))
  
  
  const AssetCard = ({
    name,
    id,
    subtitle,
    description,
    iconUrl,
    icon,
    action,
    largeHeader
  }) => {
    // use icon or iconUrl but not both
    const classes = useStyles()

    return (
      
      <Card>
        <CardHeader
        style={{height: largeHeader ? '7em' : '5em'}}
        avatar={
              <Avatar className={classes.avatar}>
              <Jdenticon value={id} />
              </Avatar>
          }
          title={name}
          subheader={subtitle}
          id={id}
        />
        <CardContent className={classes.content}>
          <Typography variant="body2" color="textPrimary" component="p">
          {description.length > 130 ? description.substring(0, 130) + ' ...' : description}
          </Typography>
        </CardContent>
        <CardActions>
        <Button size="small" color="primary" href={`assets/${id}`}>
        Action
        </Button>
        <Button size="small" color="primary" href={`assets/${id}`}>
        Details
      </Button>
        </CardActions>
      </Card>
      
    );
  };
  export default AssetCard;
  