import {
  Grid,
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Typography,
  Button,
} from "@material-ui/core";
import React from "react";

import { makeStyles } from "@material-ui/core/styles";


const useStyles = makeStyles((theme) => ({
  content: {
    minHeight: '6em',
  },
  
}))

function StatCard({largeHeader}) {
  const classes = useStyles();
  return (
    <Grid container spacing={3}>
            <Grid item md={6} xs={12} lg={4}>
            <Card>
        <CardHeader style={{height: largeHeader ? '7em' : '5em'}} color="success" stats icon />
                <CardContent className={classes.content}>
                <Typography variant="h6" color="secondary" component="p"> Usage over time</Typography>
            </CardContent>
            <CardActions>
        <Button size="small" color="primary" >
        Action
        </Button>
        <Button size="small" color="primary" >
        Details
      </Button>
        </CardActions>
      </Card>
            </Grid>
            <Grid item md={6} xs={12} lg={4}>
            <Card>
        <CardHeader style={{height: largeHeader ? '7em' : '5em'}} color="success" stats icon />
                <CardContent className={classes.content}>
                <Typography variant="h6" color="secondary" component="p"> Computing Hours Used</Typography>
            </CardContent>
            <CardActions>
        <Button size="small" color="primary" >
        Action
        </Button>
        <Button size="small" color="primary" >
        Details
      </Button>
        </CardActions>
      </Card>
            </Grid>
            <Grid item md={6} xs={12} lg={4}>
            <Card>
        <CardHeader style={{height: largeHeader ? '7em' : '5em'}} color="success" stats icon />
                <CardContent className={classes.content}>
                <Typography variant="h6" color="secondary" component="p"> Data Analyzed</Typography>
            </CardContent>
            <CardActions>
        <Button size="small" color="primary" >
        Action
        </Button>
        <Button size="small" color="primary" >
        Details
      </Button>
        </CardActions>
      </Card>
            </Grid>
            </Grid>
  );
}

export default StatCard;
