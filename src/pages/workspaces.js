import {
    Box,
    Typography,
    Grid,
    Divider,
  } from "@material-ui/core";
  import WorkspaceCard from '../components/cards/WorkspaceCard'
  import { getMenuItem } from "../menuItems.js";
  //import Basic from '../components/basic'
  import Layout from "../components/Layout";
  
  const Workspaces = ({ images, launch }) => {
    const menuItem = getMenuItem("Workspaces");
  
    return (
      <Layout title="Workspaces">
        <Box mt={4}>
        <Typography variant="h6">New Workspace</Typography>
          <Divider />
        
          <Typography variant="h6">My Workspaces</Typography>
          <Divider />
          <br />
          <Grid container spacing={3}>
            {images.results.map(({ id, name, description, start_date, end_date }, index) => (
              <Grid item key={index} md={4} xs={12} lg={4}>
                <WorkspaceCard description={description} name={name} id={id} start_date={start_date} end_date={end_date} />
              </Grid>
            ))}
          </Grid>
        </Box>
      </Layout>
    );
  };
  
  export async function getServerSideProps() {
    // fetch no longer needs to be imported from isomorphic-unfetch
    const res = await fetch("https://atmo.cyverse.org/api/v2/images?format=json");
    const images = await res.json();
    return {
      props: {
        images,
      },
    };
  }
  
  export default Workspaces;