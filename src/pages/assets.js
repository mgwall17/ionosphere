import { Box, Typography, Grid, Divider } from "@material-ui/core";
import AssetCard from "../components/cards/AssetCard";
import { getMenuItem } from "../menuItems.js";



import Layout from "../components/Layout";

const Assets = ({ images, launch }) => {
  const menuItem = getMenuItem("Assets");

  return (
    <Layout title="Assets">
      <Box mt={4}>
        <Typography variant="h6">My Assets</Typography>
        <Divider />
        <br />
        <Grid container spacing={3}>
          {images.results.map(({ id, name, description }, index) => (
            <Grid item key={index} md={6} xs={12} lg={4}>
              <AssetCard description={description} name={name} id={id}/>
            </Grid>
          ))}
        </Grid>
      </Box>
    </Layout>
  );
};

export async function getServerSideProps() {
  // fetch no longer needs to be imported from isomorphic-unfetch
  const res = await fetch("https://atmo.cyverse.org/api/v2/images?format=json");
  const images = await res.json();
  return {
    props: {
      images,
    },
  };
}

export default Assets;
