import {
    Box,
    Typography,
    Grid,
    Divider,
  } from "@material-ui/core";
  import SummaryCard from '../components/SummaryCard'
  import { getMenuItem } from "../menuItems.js";
  
  import Layout from "../components/Layout";
  
  const Credentials = (props) => {
    const menuItem = getMenuItem("Credentials");
    return (
      <Layout title="Credentials">
      <Box mt={4}>
      <Typography variant="h6">
              My Credentials
            </Typography>
            <Divider />
            <br/>
        <Grid container spacing={3}>
            <Grid item md={6} xs={12} lg={4}>
            <SummaryCard />
            </Grid>
            <Grid item md={6} xs={12} lg={4}>
            <SummaryCard />
            </Grid>
            <Grid item md={6} xs={12} lg={4}>
            <SummaryCard />
            </Grid>
            <Grid item md={6} xs={12} lg={4}>
            <SummaryCard />
            </Grid>
            <Grid item md={6} xs={12} lg={4}>
            <SummaryCard />
            </Grid>
            <Grid item md={6} xs={12} lg={4}>
            <SummaryCard />
            </Grid>
            <Grid item md={6} xs={12} lg={4}>
            <SummaryCard />
            </Grid>
            <Grid item md={6} xs={12} lg={4}>
            <SummaryCard />
            </Grid>
            <Grid item md={6} xs={12} lg={4}>
            <SummaryCard />
            </Grid>
            </Grid>
        </Box>
      </Layout>
    );
  };
  export default Credentials;
  