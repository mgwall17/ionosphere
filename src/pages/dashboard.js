import {
    Box,
    Typography,
    Grid,
    Divider,
  } from "@material-ui/core";
  import SummaryCard from '../components/SummaryCard'
  import StatCard from '../components/Cards/StatCard'
  import { getMenuItem } from "../menuItems.js";
  
  import Layout from "../components/Layout";
  
  const Page = (props) => {
    const menuItem = getMenuItem("Dashboard");
    return (
      <Layout title="Dashboard">
      <Box mt={4}>
      <Typography variant="h6">
              Overview
            </Typography>
            <Divider />
            <br/>
        <StatCard />
            <Box mt={4}>
            <Typography variant="h6">
              Workspaces
            </Typography>
            <Divider />
            <br/>
            </Box>
            <Grid container spacing={3}>
            <Grid item md={6} xs={12} lg={4}>
            <SummaryCard />
            </Grid>
            <Grid item md={6} xs={12} lg={4}>
            <SummaryCard />
            </Grid>
            <Grid item md={6} xs={12} lg={4}>
            <SummaryCard />
            </Grid>
            </Grid>
            <Box mt={4}>
            <Typography variant="h6">
              Clouds
            </Typography>
            <Divider />
            <br/>
            </Box>
            <Grid container spacing={3}>
            <Grid item md={6} xs={12} lg={4}>
            <SummaryCard />
            </Grid>
            <Grid item md={6} xs={12} lg={4}>
            <SummaryCard />
            </Grid>
            <Grid item md={6} xs={12} lg={4}>
            <SummaryCard />
            </Grid>
            </Grid>
        </Box>
      </Layout>
    );
  };
  export default Page;
  