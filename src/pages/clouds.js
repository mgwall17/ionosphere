import {
    Typography,
    Grid,
    Divider,
    Box
  } from "@material-ui/core";
  import SummaryCard from '../components/SummaryCard'
  import { getMenuItem } from "../menuItems.js";
  
  import Layout from "../components/Layout";
  
  const Clouds = (props) => {
    const menuItem = getMenuItem("Clouds");
    return (
      <Layout title="Clouds">
      <Box mt={4}>
      <Typography variant="h6">
              Available Clouds
            </Typography>
            <Divider />
            <br/>
        <Grid container spacing={3}>
            <Grid item md={6} xs={12} lg={4}>
            <SummaryCard />
            </Grid>
            <Grid item md={6} xs={12} lg={4}>
            <SummaryCard />
            </Grid>
            <Grid item md={6} xs={12} lg={4}>
            <SummaryCard />
            </Grid>
            <Grid item md={6} xs={12} lg={4}>
            <SummaryCard />
            </Grid>
            <Grid item md={6} xs={12} lg={4}>
            <SummaryCard />
            </Grid>
            <Grid item md={6} xs={12} lg={4}>
            <SummaryCard />
            </Grid>
            <Grid item md={6} xs={12} lg={4}>
            <SummaryCard />
            </Grid>
            <Grid item md={6} xs={12} lg={4}>
            <SummaryCard />
            </Grid>
            <Grid item md={6} xs={12} lg={4}>
            <SummaryCard />
            </Grid>
            </Grid>
        </Box>
      </Layout>
    );
  };
  
  export default Clouds;
  