import {
    Box,
    Typography,
    Grid,
    Divider,
  } from "@material-ui/core";
  import SummaryCard from '../components/SummaryCard'
  import { getMenuItem } from "../menuItems.js";
  
  import Layout from "../components/Layout";
  
  const Templates = (props) => {
    const menuItem = getMenuItem("Templates");
    return (
      <Layout title="Templates">
      <Box mt={4}>
      <Typography variant="h6">
              Recent Templates
            </Typography>
            <Divider />
            <br/>
        <Grid container spacing={3}>
            <Grid item md={6} xs={12} lg={4}>
            <SummaryCard />
            </Grid>
            <Grid item md={6} xs={12} lg={4}>
            <SummaryCard />
            </Grid>
            <Grid item md={6} xs={12} lg={4}>
            <SummaryCard />
            </Grid>
            <Grid item md={6} xs={12} lg={4}>
            <SummaryCard />
            </Grid>
            <Grid item md={6} xs={12} lg={4}>
            <SummaryCard />
            </Grid>
            <Grid item md={6} xs={12} lg={4}>
            <SummaryCard />
            </Grid>
            <Grid item md={6} xs={12} lg={4}>
            <SummaryCard />
            </Grid>
            <Grid item md={6} xs={12} lg={4}>
            <SummaryCard />
            </Grid>
            <Grid item md={6} xs={12} lg={4}>
            <SummaryCard />
            </Grid>
            </Grid>
        </Box>
      </Layout>
    );
  };
  export default Templates;
  