import { createMuiTheme } from "@material-ui/core/styles";

const theme = createMuiTheme({
  palette: {
    primary: {
      light: "#9c4dcc",
      main: "#6a1b9a",
      dark: "#38006b",
      contrastText: "#fff",
    },
    secondary: {
      light: "#559fdd",
      main: "#0971ab",
      dark: "#00467b",
      contrastText: "#fff",
    },
    error: {
      light: "#e57373",
      main: "#f44336",
      dark: "#9A0036",
      contrastText: "#fff",
    },
    success: {
      light: "#81C784",
      main: "#4caf50",
      dark: "#388E3C",
      contrastText: "rgba(0,0,0,.87)",
    },
    warning: {
      light: "#ffb74d",
      main: "#ff9800",
      dark: "#f57c00",
      contrastText: "rgba(0,0,0,.87)",
    },
    divider: "rgba(0,0,0,.12)",
  },
  overrides: {
    MuiListItem: {
      secondaryAction: {
        text: {
          color: "#fffff",
        },
      },
      root: {
        "& .MuiSvgIcon-root": {
          fill: "white",
        },
        "&$selected": {
          backgroundColor: "#9c4dcc",
          color: "#ffffff",
          "&:hover": {
            backgroundColor: "#9c4dcc",
          },
          "& .MuiSvgIcon-root": {
            fill: "white",
          },
        },
      },
    },
  },
});

export default theme;
